import java.util.Scanner;

public class WeeklySales {
    public static void main(String[] args) {
        Salesperson[] salesStaff;
        int jmlSales = 0;
        Scanner scan = new Scanner(System.in);

        // salesStaff[0] = new Salesperson("Jane", "Jones", 3000);
        // salesStaff[1] = new Salesperson("Daffy", "Duck", 4935);
        // salesStaff[2] = new Salesperson("James", "Jones", 3000);
        // salesStaff[3] = new Salesperson("Dick", "Walter", 2800);
        // salesStaff[4] = new Salesperson("Don", "Trump", 1570);
        // salesStaff[5] = new Salesperson("Jane", "Black", 3000);
        // salesStaff[6] = new Salesperson("Harry", "Taylor", 7300);
        // salesStaff[7] = new Salesperson("Andy", "Adams", 5000);
        // salesStaff[8] = new Salesperson("Jim", "Doe", 2850);
        // salesStaff[9] = new Salesperson("Walt", "Smith", 3000);

        System.out.println("Masukan jumlah salesperson :");
        jmlSales = scan.nextInt();
        salesStaff = new Salesperson[jmlSales];

        System.out.println();
        for (int i = 0; i < jmlSales; i++) {
            String NamaDepan, NamaBelakang;
            int sales;
            System.out.println("Masukan nama depan karyawan ke " + (i + 1) + ": ");
            NamaDepan = scan.next();
            System.out.println("Masukan nama belakang karyawan ke " + (i + 1) + ": ");
            NamaBelakang = scan.next();
            System.out.println("Masukan sales depan karyawan ke " + (i + 1) + ": ");
            sales = scan.nextInt();

            salesStaff[i] = new Salesperson(NamaDepan, NamaBelakang, sales);
            System.out.println();

        }

        scan.close();

        Sorting.insertionSort(salesStaff);
        System.out.println("\nRanking of Sales for the Week\n");
        for (Salesperson s : salesStaff) {
            System.out.println(s);
        }
    }
}
